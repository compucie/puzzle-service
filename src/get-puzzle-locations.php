<?php

define("DATE_STRING", date("Y-m-d"));

cleanupOldPuzzleDirs();
createTodaysPuzzleDirIfNotExist();
echoPuzzlePaths();

function cleanupOldPuzzleDirs()
{
    $puzzleDirs = preg_grep("/[0-9]{4}-[0-9]{2}-[0-9]{2}/", scandir(".."));
    foreach ($puzzleDirs as $dirName) {
        $dir = "../$dirName";

        if (!is_dir($dir)) {
            continue;
        }

        $isEmpty = function ($dir) {
            $content = array_diff(scandir($dir), array('.', '..', '@eaDir', 'Thumbs.db'));
            return count($content) == 0;
        };
        if ($isEmpty($dir)) {
            rmdir($dir);
            continue;
        }

        if ($dirName != DATE_STRING) {
            rename($dir, "../archive/$dirName");
            continue;
        }
    }
}

function createTodaysPuzzleDirIfNotExist()
{
    if (!is_dir("../" . DATE_STRING)) {
        mkdir("../" . DATE_STRING);
    }
}

/**
 * Reply with all .png and .jpg files in todays folder.
 */
function echoPuzzlePaths()
{
    $filenames = preg_grep("/.*\.(png|jpg)/", scandir("../" . DATE_STRING));

    header("Content: application/json;");
    echo json_encode(array_map(
        function (string $filename) {
            return "https://{$_SERVER['HTTP_HOST']}/" . DATE_STRING . "/$filename";
        },
        $filenames
    ));
}
